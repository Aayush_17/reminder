# Load libraries required by the Evernote OAuth
require 'oauth'
require 'oauth/consumer'

# Load Thrift and Evernote Ruby libraries
require "evernote_oauth"

# Client Credentials
OAUTH_CONSUMER_KEY = "your key"
OAUTH_CONSUMER_SECRET = "your secret"

# Connect to Sandbox Server?
SANDBOX = true
