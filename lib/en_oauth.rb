require 'sinatra'  # Webapp framework
enable :sessions  # Enable cookie based sessions (part of sinatra)

### Load our dependencies and configuration settings

# Include current directory in LOAD_PATH (to load our config file)
$LOAD_PATH.push(File.expand_path(File.dirname(__FILE__)))
# Include config file
require "evernote_config.rb"


# Runs when app is initialized and verify that the keys are not empty
before do
  if OAUTH_CONSUMER_KEY.empty? || OAUTH_CONSUMER_SECRET.empty?
    halt '<span style="color:red">Edit evernote_config.rb and replace both these variables with your keys.</span>'
  end
end


###### HELPER FUNCTIONS ######

# Stores the authentication token used to Evernote API calls
# Since our webapp is stateless, this value is stored in Sinatra's session
def auth_token
  session[:access_token].token if session[:access_token]
end

# Represents the instance of EvernoteOAuth::Client.
# Instantiated using our tokens
def client
  @client ||= EvernoteOAuth::Client.new(token: auth_token, consumer_key:OAUTH_CONSUMER_KEY, consumer_secret:OAUTH_CONSUMER_SECRET, sandbox: SANDBOX)
end

### Services of Evernote cloud API

# User and account information
def user_store
  @user_store ||= client.user_store
end

# User's notes, notebooks, etc
def note_store
  @note_store ||= client.note_store
end

# Represents a single Evernote User instance retrieved from the API
def en_user
  user_store.getUser(auth_token)
end

# Collection of Notebooks objects
def notebooks
  @notebooks ||= note_store.listNotebooks(auth_token)
end


def total_note_count

  # Empty instance of NoteFilter. We don't add an additional filter criteria
  # since we want to query the whole account
  # More info here - https://dev.evernote.com/doc/reference/NoteStore.html#Struct_NoteFilter
  filter = Evernote::EDAM::NoteStore::NoteFilter.new

  # Returns NoteCollectionCounts: A hash of notebook GUIDs mapped to the
  # number of notes in the corresponding notebook.
  counts = note_store.findNoteCounts(auth_token, filter, false)  # false parameter says that we don't want notes from trash

  # Iterating over all our Notebooks
  # We ask counts for no. of notes in that notebook -- using its GUID --
  # and return the total
  notebooks.inject(0) do |total_count, notebook|
    total_count + (counts.notebookCounts[notebook.guid] || 0)
  end
end


###### ROUTES ######

# Displays the index template and prompts the user to begin the
# OAuth authentication process
get '/' do
  erb :index
end

# Clear session object and redirect to / so the user can restart
# the process
get '/reset' do
  session.clear
  redirect '/'
end

# Collects info about the user's account using helper methods from above
# Adds that info to the current session object and renders the index template
# If errors, the error is sent to the error template
get '/list' do
  begin
    # Get notebooks
    session[:notebooks] = notebooks.map(&:name)
    # Get username
    session[:username] = en_user.username
    # Get total note count
    session[:total_notes] = total_note_count
    erb :index
  rescue => e
    @last_error = "Error listing notebooks: #{e.message}"
    erb :error
  end
end


###### OAUTH ######

### Step 1: Get request token from API to request an authentication token

# Take current URL and replace "requesttoken" portion with "callback" to build
# our callback URL.
# Then, call the method to request token and send callback_url to tell the API
# where to send the response
# Redirect to /authorize for the next step
get '/requesttoken' do
  callback_url = request.url.chomp("requesttoken").concat("callback")
  begin
    session[:request_token] = client.request_token(:oauth_callback => callback_url)
    redirect '/authorize'
  rescue => e
    @last_error = "Error obtaining temporary credentials: #{e.message}"
    erb :error
  end
end

### Step 2: Using the request token, send user to Evernote Site to authenticate
### with their login credentials and authorize our app for access.

# Verify that request_token exists, then get authorize_url from the tokens
# and send the user there to login with Evernote and authorize our app for access.
# After the process, they're sent back to /callback which we defined in the
# /requesttoken method
get '/authorize' do
  if session[:request_token]
    redirect session[:request_token].authorize_url
  else
    # You shouldn't be invoking tihs if you dont ahve a request tokens
    @last_error = "Request token not set"
    erb :error
  end
end


### Step 3: Evernote redirects user back to our callback url and
# append, among other things, the authentication token to make calls

# Verify that oauth_verifier is set in URL or request_token value is present in sessions
# If either one is true, we include oauth_verifier parameter in our session.
# Next, we extract access_token and add it to session.
# If all goes well, we send user to /list and the user will see their
# username, no. of notes in account, and name of each notebook.
get '/callback' do
  unless params['oauth_verifier'] || session['request_token']
    @last_error = "Content owner did not authorize the temporary credentials"
    hald erb :error
  end
  session[:oauth_verifier] = params['oauth_verifier']
  begin
    session[:access_token] = session[:request_token].get_access_token(:oauth_verifier => session[:oauth_verifier])
    redirect '/list'
  rescue => e
    @last_error = 'Error extracting access token'
    erb :error
  end
end


###### TEMPLATES ######

# Index: Show authentication link and display result if successfull
# Error: Print out the error

__END__

@@ index
<html>
  <head>
    <title>Evernote Reminder App</title>
  </head>
  <body>
    <a href="/requesttoken">Click here</a> to authenticate this application using OAuth.
    <% if session[:notebooks] %>
    <hr />
    <h3>The current user is <%= session[:username] %> and there are <%= session[:total_notes] %> notes in their account</h3>
    <br />
    <h3>Here are the notebooks in this account:</h3>
      <ul>
      <% session[:notebooks].each do |notebook| %>
        <li><%= notebook %></li>
      <% end %>
    </ul>
    <% end %>
  </body>
</html>


@@ error
<html>
  <head>
    <title>Evernote Ruby Example App &mdash; Error</title>
  </head>
  <body>
    <p>An error occurred: <%= @last_error %></p>
    <p>Please <a href="/reset">start over</a>.</p>
  </body>
</html>
