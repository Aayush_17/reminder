Step 1: Get Evernote API Key from https://dev.evernote.com/support/api_key.php

Step 2: Create Evernote sandbox account from http://sandbox.evernote.com/

Step 3: Check if Ruby is installed through "ruby -v" or else install it http://www.ruby-lang.org/

Step 4: Download RubyGems from http://rubygems.org/pages/download

Step 5: Execute "gem --version" and "gem update --system" (might require sudo)

Step 6: Install the webdev framework Sinatra using "gem install sinatra"

Step 7: Install Evernote SDK for Ruby using "gem install evernote_oauth"

Note: The app runs on localhost:4567
